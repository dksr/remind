#############################################################################
#
#   Author   : Krishna Sandeep Reddy Dubba
#   Email    : scksrd@leeds.ac.uk
#   Institute: University of Leeds, UK
#
#############################################################################

"""Some CAD_120 dataset utilities"""

import cPickle as pickle
import itertools
import logging
import os, sys

sys.path.append('../../')
from learn.ilp_utils.logic import Expr, expr
from learn.ilp_utils.hyp   import get_allen_relation, temporal_inverse_substitutes

log = logging.getLogger("lam.learn.utils.CAD_120_utils.episodes2prolog")

def episodes2prolog(episodes_file, output_dir, obj_type_dict=None, tag='sub_act'):
    episodes = pickle.load(open(episodes_file))
    subjects = {}
    all_rels = {}
    
    # Collect the data
    for file_name in episodes:
        print file_name
        (subj, activity, sub_act, act_id, min_rows, max_rows, file_tag) = file_name.split('__')
        subj = subj.lower()
        if file_tag != tag:
            continue
        if subj not in subjects:
            subjects[subj] = {}
        if sub_act not in subjects[subj]:
            subjects[subj][sub_act] = {}
        subjects[subj][sub_act][act_id] = {}
        intervals = []
        for epi in episodes[file_name]:
            for (obj1_id, obj1_type, obj2_id, obj2_type, rel, start, end) in episodes[file_name][epi]:
                if rel not in subjects[subj][sub_act][act_id]:
                    subjects[subj][sub_act][act_id][rel] = []
                    
                if obj1_type not in obj_type_dict:
                    print obj1_type
                if obj2_type not in obj_type_dict:
                    print obj2_type
                    
                if rel not in all_rels:
                    all_rels[rel] = True
                    
                arg1 = Expr(obj_type_dict.get(obj1_type,obj1_type), obj1_type.lower() + '_' + obj1_id)
                arg2 = Expr(obj_type_dict.get(obj2_type,obj2_type), obj2_type.lower() + '_' + obj2_id)
                #arg3 = Expr('intv',[start, end])
                arg3 = Expr('intv', 't'+str(start) + '_t' + str(end))
                intervals.append((start,end,arg3))
                subjects[subj][sub_act][act_id][rel].append(Expr(rel,[arg1, arg2, arg3]))
        for [(is1,ie1,intv1_str),(is2,ie2,intv2_str)] in itertools.combinations(intervals,2):
            temporal_rel = get_allen_relation(is1, ie1, is2, ie2)            
            if temporal_rel in temporal_inverse_substitutes:
                temporal_rel = temporal_inverse_substitutes[temporal_rel]
                arg1 = intv2_str
                arg2 = intv1_str
            else:    
                arg1 = intv1_str
                arg2 = intv2_str
            if temporal_rel == 'equal' or temporal_rel == 'starts' or temporal_rel == 'finishes':
                # Ignore these 3 temporal relations
                continue                
            if temporal_rel not in subjects[subj][sub_act][act_id]:
                subjects[subj][sub_act][act_id][temporal_rel] = []
            subjects[subj][sub_act][act_id][temporal_rel].append(Expr(temporal_rel,[arg1, arg2]))            
    
    concept_name = None    
    # Generate train and test files
    for subj_i in subjects:
        for sub_act_i in subjects[subj_i]:
            concept_name = sub_act_i
            # We write test and train (both pos and neg) interpretations in a single file
            train_test_file = open(os.path.join(output_dir, \
                                                subj_i + '_' + sub_act_i + '_train_test.pl'),'w')
            data_str   = ''
            header_str = ''
            concept_name_args = None
            for subj_j in subjects:
                if subj_i == subj_j:
                    test = True
                else:
                    test = False
                
                for sub_act_j in subjects[subj_j]:                    
                    if sub_act_j == sub_act_i:
                        pos = True                        
                    else:
                        pos = False
                    for act_id in subjects[subj_j][sub_act_j]:
                        epi_data = subjects[subj_j][sub_act_j][act_id]
                        interpretation_str  = ''
                        temporal_data_str   = ''
                        interpretation_arg1 = ''
                        interpretation_arg2 = ''
                        for rel in epi_data:
                            for fact in epi_data[rel]:
                                for arg in fact.args:
                                    if arg.op == 'obj::body_part::hand::lh':
                                        interpretation_arg1 = arg.args[0]
                                    if arg.op == 'obj::body_part::hand::rh':
                                        interpretation_arg2 = arg.args[0]
                                rel_expr = repr(fact) + '.\n'
                                if rel in temporal_inverse_substitutes.values():
                                    temporal_data_str += rel_expr
                                else:    
                                    interpretation_str += rel_expr
                        interpretation_str += temporal_data_str + '\n'
                        
                        if test and pos:
                            # If its a test case, append 'test_' before the relation name
                            # if it is a negative example, append ':-' before relation name
                            ex_expr = repr(Expr('test_' + sub_act_j, \
                                                [interpretation_arg1, interpretation_arg2, subj_j + \
                                                 '_' + sub_act_j + '_' + act_id])) + '.\n'
                        elif test and not pos:
                            ex_expr = ':-' + repr(Expr('test_' + concept_name + '_' + sub_act_j, \
                                                [interpretation_arg1, interpretation_arg2, subj_j + \
                                                 '_' + sub_act_j + '_' + act_id])) + '.\n'
                        elif not test and pos:
                            ex_expr = repr(Expr(sub_act_j, \
                                                [interpretation_arg1, interpretation_arg2, subj_j + \
                                                 '_' + sub_act_j + '_' + act_id])) + '.\n'
                        elif not test and not pos:    
                            ex_expr = ':-' + repr(Expr(concept_name + '_' + sub_act_j, \
                                                [interpretation_arg1, interpretation_arg2, subj_j + \
                                                 '_' + sub_act_j + '_' + act_id])) + '.\n'
                                                        
                        interpretation_str = ex_expr + interpretation_str
                    data_str += interpretation_str
                    
            header_str += ':- modeh(1, ' + concept_name + '(obj-, obj-, intp_id-)).\n'
            for relation in all_rels:
                header_str += ':- modeb(3, ' + relation + '(obj+, obj-, intv-)).\n'
                header_str += ':- modeb(3, ' + relation + '(obj-, obj+, intv-)).\n'
                header_str += ':- modeb(3, ' + relation + '(obj+, obj+, intv-)).\n'                
            header_str += ':- modeb(4, meets(intv+, intv+)).\n'
            header_str += ':- modeb(4, overlaps(intv+, intv+)).\n'
            header_str += ':- modeb(4, during(intv+, intv+)).\n'
            header_str += '\n'
            train_test_file.write(header_str)
            train_test_file.write(data_str)
            train_test_file.close()        

    print 'DONE'    
    return subjects

if __name__ == '__main__':
    import os
    episodes_file = os.path.join(os.environ['CAD_DATA_DIR'],'relational_graph_data/sub_act_episodes.p')
    output_dir    = os.path.join(os.environ['CAD_DATA_DIR'],'prolog_rel_data')
    
    obj_type_dict = {'LH':['obj', 'body_part', 'hand', 'lh'],
                     'RH':['obj', 'body_part', 'hand', 'rh'],
                     'LS':['obj', 'body_part', 'shoulder', 'ls'],
                     'microwave':['obj', 'appliance', 'mw'],
                     'cup':['obj', 'cutlery', 'cup'],
                     'plate':['obj', 'cutlery', 'plate'],
                     'bowl':['obj', 'cutlery', 'bowl'],
                     'milk':['obj', 'food', 'milk'],
                     'book':['obj', 'utility', 'book'],
                     'box':['obj', 'utility', 'box'],
                     'cloth':['obj', 'utility', 'cloth'],
                     'unknown':['obj'],
                     'medcinebox':['obj', 'utility', 'box', 'medicinebox'],
                     'remote':['obj', 'utility', 'remote']}
    
    episodes2prolog(episodes_file, output_dir, obj_type_dict)
    
    """
    :- modeh(1, serve_coffee(-space)).
    :- modeb(1, pickup(+space, -int)).
    :- modeb(1, putdown(+space, -int)).
    :- modeb(1, con(+space, -int)).
    :- modeb(3, dis(+num, -int)).
    :- modeb(3, meetsdis(+int, +int)).
    :- modeb(3, overlaps(+int, +int)).
    :- modeb(3, during(+int, +int)).
    """