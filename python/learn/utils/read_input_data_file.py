def read_rel_data(rel_file):
    import logging
    import os
    import re
    import sys
    
    from learn.ilp_utils.hyp   import Modes
    from learn.ilp_utils.logic import Expr, expr    
    
    log = logging.getLogger("remind.learn.utils.read_input_data")
    
    modeh    = []
    modeb    = []
    rel_data = {}
    train_test_flag    = 'train'
    class_predicate    = None
    pos_example_flag   = True
    example_data_start = False
    rel_data['modes'] = []
    rel_data['train'] = {}
    rel_data['test']  = {}
    rel_data['train']['neg'] = {}    
    rel_data['train']['pos'] = {}
    rel_data['test']['neg']  = {}
    rel_data['test']['pos']  = {}
    
    log.info('Reading prolog relational data file ...')
    input_file = open(rel_file)
    
    mode_pattern        = re.compile(':- mode(\w)\((\d*), (.*)\((.*)\)\).')
    neg_ex_fact_pattern = re.compile(':-(\w*)\((.*)\).')
    fact_pattern        = re.compile('(\w*)\((.*)\).')
    
    for line in input_file:
        line = line.strip()
        if line == '' or line.startswith('%'):
            continue
        if line.find(':- mode') != -1:
            (hORb, recall, predicate, mode_args) = mode_pattern.findall(line)[0]
            mode_args = mode_args.split(', ')
            expr_str = predicate + '(' + ','.join(mode_args) + ')'
            # We don't use the symbol # in python as it is used for comments
            expr_str = expr_str.replace('#', '$')
            if hORb == 'h':
                class_predicate = predicate
                modeh = [Modes(1, expr(expr_str))]
            elif hORb == 'b':
                modeb.append(Modes(1, expr(expr_str), False))
        elif line.startswith(class_predicate):
            # Positive train example
            (predicate, pos_example_args) = fact_pattern.findall(line)[0]
            train_test_flag    = 'train'
            example_data_start = True
            pos_example_flag   = True
            pos_example_args   = pos_example_args.replace(',','__')
            pos_example_label  = class_predicate + '__' + pos_example_args
            rel_data[train_test_flag]['pos'][pos_example_label] = {}
        elif line.startswith(':-' + class_predicate):
            # Negative train example
            (predicate, neg_example_args) = fact_pattern.findall(line)[0]
            train_test_flag    = 'train'            
            example_data_start = True
            pos_example_flag   = False
            neg_example_label  = neg_example_args.split(',')[-1]
            rel_data[train_test_flag]['neg'][neg_example_label] = {}
        elif line.startswith('test_' + class_predicate):
            # Positive test example
            (predicate, pos_example_args) = fact_pattern.findall(line)[0]
            train_test_flag    = 'test'
            example_data_start = True
            pos_example_flag   = True
            pos_example_args   = pos_example_args.replace(',','__')
            pos_example_label  = class_predicate + '__' + pos_example_args
            rel_data[train_test_flag]['pos'][pos_example_label] = {}            
        elif line.startswith(':-test_' + class_predicate):
            # Negative test example
            (predicate, neg_example_args) = fact_pattern.findall(line)[0]
            train_test_flag    = 'test'
            example_data_start = True
            pos_example_flag   = False
            neg_example_label  = neg_example_args.split(',')[-1]
            rel_data[train_test_flag]['neg'][neg_example_label] = {}            
            
        elif example_data_start:
            (predicate, fact_args) = fact_pattern.findall(line)[0]
            fact_args = fact_args.split(', ')
            expr_str  = predicate + '(' + ','.join(fact_args) + ')'
            if pos_example_flag:
                if predicate not in rel_data[train_test_flag]['pos'][pos_example_label]:
                    rel_data[train_test_flag]['pos'][pos_example_label][predicate] = []
                rel_data[train_test_flag]['pos'][pos_example_label][predicate].append(expr(expr_str))
            else:
                if predicate not in rel_data[train_test_flag]['neg'][neg_example_label]:
                    rel_data[train_test_flag]['neg'][neg_example_label][predicate] = []
                rel_data[train_test_flag]['neg'][neg_example_label][predicate].append(expr(expr_str))
    
    input_file.close()
    rel_data['modes'] = [modeh, modeb]
    log.info('Finished importing data from prolog relational file')
    return rel_data

if __name__ == "__main__":
    import os, sys
    
    sys.path.append('../../')
    
    prolog_input_dir = os.path.join(os.environ['CAD_DATA_DIR'],'prolog_rel_data')
    
    #rel_file = '/home/csunix/visdata/cofriend/sandeep_code/remind/data/example.pl'
    rel_file = os.path.join(prolog_input_dir, 'subject1_microwave_train_test.pl')
    read_rel_data(rel_file)