#############################################################################
#
#   Author   : Krishna Sandeep Reddy Dubba
#   Email    : scksrd@leeds.ac.uk
#   Institute: University of Leeds, UK
#
#############################################################################
import ConfigParser    
import cPickle as pickle    
import logging
import os
import sys
import time

sys.path.append(os.path.dirname(__file__))

def learn_rel_models(input_dir):
    from learn.utils.read_input_data_file import read_rel_data
        
    for ifile in os.listdir(input_dir):
        all_prolog_modules = read_rel_data(os.path.join(input_dir, ifile))
        learn_event_worker(ifile, all_prolog_modules=all_prolog_modules)

def learn_event_worker(ifile, argv=None, all_prolog_modules=None):

    def m_process_worker(input_queue, output_queue, ilp_best_first_graph_search):
        mp_ilp = input_queue.get()
        (solution_clause, STATUS) = ilp_best_first_graph_search(mp_ilp, mp_ilp.f)
        output_queue.put((solution_clause, STATUS))        
        
    def filter_temporal_bc(spatial_bc_body, temporal_bc_body):        
        """Get all temporal terms from temporal_bc_body that has intervals in any spatial relation
        in spatial_bc_body
        """
        from base.ilp.hyp import sp_rel
        
        temporal_args = {}
        valid_temporal_terms = []
        # Collect all temporal intervals from spatial relations
        for term in spatial_bc_body:
            if term.op in sp_rel:
                temporal_args.update({term.args[-1]:1})
        valid_temporal_args = {}            
        # Now check if the two intervals of temporal term are in the collected temporal intervals
        for term in temporal_bc_body:
            if term.args[0] != term.args[1] and term.args[0] in temporal_args and term.args[1] in temporal_args:
                # redundant temporal relation for (b,a) is present for interval (a,b). So discard one relation. 
                if tuple(term.args) not in valid_temporal_args:
                    valid_temporal_args.update({tuple(term.args):1})
                    valid_temporal_args.update({(term.args[1],term.args[0]):1})                
                    valid_temporal_terms.append(term)
            
        return valid_temporal_terms        

    def get_solution_hyp(modeh, modeb, prolog, constants, pos_prolog_modules, neg_prolog_modules,\
                         DEBUG=True):
        
        # Removed data_f as we don't need bg_KB now
        import copy
        import gc
        import random        
        import re
       
        from multiprocessing     import Process, Queue, current_process, freeze_support
       
        from base.utils.search   import ilp_best_first_graph_search
        from base.utils.cpu_load import mem_avail
        from learn.ilp_utils.logic import Expr
        from learn.ilp_utils.hyp import Clause, construct_clause_from_example, variablize_clause
        from learn.ilp_utils.ilp import ILP
                
        pos_blankets = pos_prolog_modules.values()
        neg_blankets = neg_prolog_modules.values()
        i = 0 #test_data_list[0]
        solution_hyp   = []    
        inTerms        = {}
        sol            = {}
        pos_eval_time  = 0
        neg_eval_time  = 0
        # Need to call this in a loop until all positive examples are covered.
        # Then append all the solutions into a hypothesis. 
        no_sol_count = 0
        # Repeat until all positive examples are covered
        input_data_queue  = Queue()
        output_data_queue = Queue()
        
        train_pos_ex_expr = []
        for ex in pos_prolog_modules:
            ex_fields = ex.split('__')
            train_pos_ex_expr.append(Expr(ex_fields[0],ex_fields[1:]))
        
        while len(pos_prolog_modules) != 0:
            ilp = ILP(constants[:-1], None, modeh, modeb, pos_prolog_modules, neg_prolog_modules, \
                      prolog, train_pos_ex_expr)
            
            log.info('')
            log.info(MBT + 'Examples yet to cover: ' + RESET + RBB + repr(len(ilp.pos_ex)) + RESET)
            
            # A different initial clause is generated based on pos_ex remaining
            ilp.bottom_clause = None
               
            # no_sol_count gives the number of example for which bottom_clause has non zero length body
            (no_sol_count, initial_clause_flag) = ilp.generate_initial_generic_clause(no_sol_count)
            if not initial_clause_flag:
                log.info(RBB + class_label + ' test_vid-' + repr(i) + ': Could not find proper solution. No proper' \
                         + ' bottom clause was constructed :-(' + RESET)
                break 
        
            #ilp.bottom_clause = ilp.full_bottom_clause   
            (solution_clause, STATUS) = ilp_best_first_graph_search(ilp, ilp.f)
            if DEBUG:
                (solution_clause, STATUS) = ilp_best_first_graph_search(ilp, ilp.f)
            else: 
                input_data_queue.put(ilp)
                log.info(GBT + 'Finding hyp in a seperate process ...' + RESET)
                Process(target=m_process_worker, args=(input_data_queue, output_data_queue, ilp_best_first_graph_search)).start()
                (solution_clause, STATUS) = output_data_queue.get()
        
            solution_clause = solution_clause.state
            if STATUS == OUT_OF_MEMORY:
                break
                          
            (score, pos_cov, neg_cov, tot_pos) = solution_clause.score
            if len(pos_cov) is not 0:
                no_sol_count = 0
                solution_hyp.append(solution_clause)
                for key in ilp.inTerms.keys():
                    try:
                        inTerms[key].append(ilp.inTerms[key])
                    except KeyError:
                        inTerms[key] = [ilp.inTerms[key]]                                           
                remove_exp      = []
                # Collect all positive examples that are to be removed and then remove
                for k in pos_cov:
                    #remove_exp.append(train_pos_ex.clauses[modeh[0].predicate][k])
                    pos_prolog_modules.pop(k)
                # Update the pos examples and blankets
                #train_pos_ex.clauses[modeh[0].predicate] = train_pos_ex_dict.values()
            else:
                no_sol_count += 1
                if no_sol_count == len(train_pos_ex_expr):
                    log.info(RBT + event + ' test_vid-' + repr(i) + ': Could not find proper solution :-(' + RESET)
                    # Delete ILP to save memory 
                    pos_eval_time += ilp.pos_eval_time
                    neg_eval_time += ilp.neg_eval_time
                    break
            # Delete ILP to save memory 
            pos_eval_time += ilp.pos_eval_time
            neg_eval_time += ilp.neg_eval_time

        return [solution_hyp, inTerms, (pos_eval_time, neg_eval_time)]        

    
    ######   MAIN  ########
    from os import path
    from optparse import OptionParser
    
    cfg_parser = ConfigParser.ConfigParser()
    cfg_parser.read('config.cfg')       
    
    import base.utils.psutil as psutil
        
    from base.utils.pyswip        import Prolog, PrologError
    from base.base_constants      import RBB, GBB, YBB, BBB, RBT, CBT, YBT, MBT, GBT, \
                                         GT, YT, MT, RT, CT, RESET, OUT_OF_MEMORY, NORMAL
    from base.utils.Logging       import initialize_logging
    from base.utils.cpu_load      import mem_avail
    from learn.ilp_utils.logic    import FolKB, Expr
    from learn.ilp_utils.ilp      import ILP
    from learn.utils.prolog_utils import assert_example_to_prolog
    
    # set up logging to file - see previous section for more details
    # define a Handler which writes INFO messages or higher to the sys.stderr
    logging.basicConfig(level=logging.DEBUG,
                        format='[%(asctime)s] %(name)s:%(funcName)s:%(lineno)d %(levelname)s - %(message)s',
                        datefmt='%H:%M:%S',
                        filename='/usr/not-backed-up/tmp/remind.log',
                        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    # set a format which is simpler for console use
    formatter = logging.Formatter('[%(asctime)s] %(name)s:%(funcName)s:%(lineno)d %(levelname)s - %(message)s','%H:%M:%S')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)  
    
    # Setup logger format and output locations
    log = logging.getLogger("remind")
    start_time = time.asctime()
    log.info(" ")
    log.info(" ")
    log.info("START TIME: " + BBB + start_time + RESET)
    
    try:
        sol_file = ('/tmp/remind_out.p')
        
        var_depth    = eval(cfg_parser.get('PARAMETERS', 'var_depth'))        
        max_terms    = eval(cfg_parser.get('PARAMETERS', 'max_terms'))
        sp_max_terms = eval(cfg_parser.get('PARAMETERS', 'sp_max_terms'))
        max_bc_length    = eval(cfg_parser.get('PARAMETERS', 'max_bc_length'))
        num_refine_ops   = eval(cfg_parser.get('PARAMETERS', 'num_refine_ops'))
        min_pos_examples = eval(cfg_parser.get('PARAMETERS', 'min_pos_examples'))
        temp_rel_mandatory  = eval(cfg_parser.get('PARAMETERS', 'temporal_rel_mandatory'))
        # Should we combine the spatial and temporal terms while constructing the hyp?
        combine_spatial_temporal = eval(cfg_parser.get('PARAMETERS', 'combine_spatio_temporal'))   
        min_score_to_expand_hyp  = eval(cfg_parser.get('PARAMETERS', 'min_score_to_expand_hyp'))
        MAIL_FLAG = eval(cfg_parser.get('PARAMETERS', 'mail_flag'))
        ONLY_ERROR_MAIL = eval(cfg_parser.get('PARAMETERS', 'only_error_mail'))
        mail_id = cfg_parser.get('MAIL', 'to_mail')  
        EVALUATION               = eval(cfg_parser.get('PARAMETERS', 'evaluation'))
       
        rel_data_file = cfg_parser.get('INPUT', 'rel_file')
 
        constants = (var_depth, max_terms, sp_max_terms, num_refine_ops, max_bc_length, \
                     min_score_to_expand_hyp, combine_spatial_temporal)
        
        log.info("VAR_DEPTH         : " + repr(var_depth))        
        log.info("MAX_TERMS         : " + repr(max_terms))
        log.info("MAX_SPATIAL_TERMS : " + repr(sp_max_terms))
        log.info("MIN_POS_EXAMPLES  : " + repr(min_pos_examples))
        log.info("NUM_REFINE_OPS    : " + repr(num_refine_ops))
        log.info("MAX_BOTTOM_CLAUSE_LENGTH: " + repr(max_bc_length))
        log.info("MIN_SCORE_TO_EXPAND_HYP : " + repr(min_score_to_expand_hyp))
        log.info("TEMPORAL_REL_MANDATORY  : " + repr(temp_rel_mandatory))
        log.info("COMBINE_SPATIO_TEMPORAL : " + repr(combine_spatial_temporal))             
        log.info("")
        log.info("")
        log.info(YBT + "Data PATHS:" + RESET)
        log.info("SOLUTION_FILE        : " + sol_file)
        log.info("")

        (modeh, modeb)      = all_prolog_modules['modes']
        pos_prolog_modules  = all_prolog_modules['train']['pos']
        neg_prolog_modules  = all_prolog_modules['train']['neg']
        test_prolog_modules = all_prolog_modules['test']        
        
        # Initialize prolog engine
        prolog = Prolog()
        #str2num_file = path.join(os.path.dirname(__file__), 'base', 'utils', 'str2num.pl')
        #prolog.consult(str2num_file)
        
        for ex in pos_prolog_modules:
            assert_example_to_prolog(pos_prolog_modules[ex], prolog, ex)
    
        for ex in neg_prolog_modules:
            assert_example_to_prolog(neg_prolog_modules[ex], prolog, ex)

        sol           = {}
        pos_eval_time = 0
        neg_eval_time = 0
        
        [solution_hyp, inTerms, eval_time] = get_solution_hyp(modeh, modeb, \
                                                              prolog, constants,\
                                                              pos_prolog_modules,\
                                                              neg_prolog_modules,\
                                                              test_prolog_modules)
        pos_eval_time = eval_time[0]
        neg_eval_time = eval_time[1]
    
        # Test the HYP        
        sol[event] = {}
        sol[event][k_fold_count] = {}
        sol[event][k_fold_count]['tp'] = []
        sol[event][k_fold_count]['fp'] = []
        sol[event][k_fold_count]['tn'] = []
        sol[event][k_fold_count]['fn'] = []
        sol[event][k_fold_count]['test_results'] = {}
        sol[event][k_fold_count]['hyp'] = solution_hyp[event][0]
        sol[event][k_fold_count]['inTerms'] = inTerms[event][0]
        
        pickle.dump(sol, open(sol_file, 'w'), 1) 
        test_time_start = time.time()
        
        total_test_vids = 0 
        log.info(len(test_prolog_modules.keys()))
        log.info(len(test_data_key))
        for test_prolog_module in test_prolog_modules:
            total_test_vids += 1
            test_vid =  test_prolog_module.split('_test')[0]
            bg_KB = FolKB()
            bg_KB.clauses = test_prolog_modules[test_prolog_module][1]
            intvs = []
                
            for intv in bg_KB.clauses['int']:
                intvs.append(intv.args[0].op)
                intvs.append(intv.args[1].op)
            if len(intvs) == 0:
                continue
            start_frame = min(intvs)
            end_frame   = max(intvs)
          
            intv = Expr('int', [start_frame, end_frame])
            test_pos_ex = [Expr(event, intv)]
            pos_vid_data = []
            
            test_prolog_blanket = test_prolog_modules[test_prolog_module][1]
            sol[event][test_vid] = {}
            assert_blanket_to_prolog_str_int(test_prolog_blanket, prolog, test_prolog_module)
               
            # The test vid data. This is used for negative score in testing
            test_blanket_data = [[(test_prolog_module, test_prolog_blanket),],]
            
            # Some videos does not have event instances, in that case we don't have
            # postive test example
            #pos_ex = test_pos_ex
            ilp = ILP(constants[:-1], color_map_file, event, modeh, modeb, None, [], test_blanket_data, prolog)
            #ilp = ILP(constants[:-1], color_map_file, event, modeh, modeb, pos_ex, [], test_blanket_data, prolog)
            #ilp = ILP(constants[:-1], color_map_file, event, modeh, modeb, pos_ex, test_pos_vid_data, test_neg_vid_data, prolog)
      
            # ilp.pos_ex_intv is used to see whether intervals are in positive area
            #for ex in pos_ex.clauses[modeh[0].predicate]:
                #ilp.pos_ex_intv.append((ex.args[-1].args[0].op,ex.args[-1].args[1].op))
                
            #for ex in test_pos_ex[test_vid]:
            for ex in test_pos_ex:
                #ilp.pos_ex_intv.append((ex.args[-1].args[0].op,ex.args[-1].args[1].op))
                # Since there is only one arg in the example, i.e. interval, no need to look that deep.
                ilp.pos_ex_intv.append((ex.args[0].op, ex.args[1].op))
    
            dup_sol_hyp = []
            #if not EVALUATION:                    
                #if event + '_' in test_vid:
                    #log.info('Testing hyp for video ' + GBT + repr(test_vid) + RESET)
                #else:
                    #log.info('Testing hyp for video ' + RBT + repr(test_vid) + RESET)
                
            hit = False    
            for hyp in solution_hyp[event][0]: #[test_vid]:
                # Loop as there might be multiple hyps (disjunction)
                dup_hyp = hyp.copy()
                ilp.calc_score(dup_hyp,test=True)                    
                dup_sol_hyp.append(dup_hyp)
                # See if the hyp satisfies test video
                if dup_hyp.get_score()[1] > 0:
                    hit = True
            
            if event + '_' in test_vid and hit:        
                sol[event][k_fold_count]['tp'].append(1)
            elif event + '_' in test_vid and not hit:        
                sol[event][k_fold_count]['tp'].append(0)
                sol[event][k_fold_count]['fn'].append(1)
            elif event + '_' not in test_vid and hit:        
                sol[event][k_fold_count]['fp'].append(1)
            elif event + '_' not in test_vid and not hit:        
                sol[event][k_fold_count]['fp'].append(0)
                sol[event][k_fold_count]['tn'].append(1)
                
            sol[event][k_fold_count][test_vid] = {}    
            sol[event][k_fold_count]['test_results'][test_vid] = hit
            #if not EVALUATION:
                #if hit:
                    #log.info('Test result for ' + GBT + repr(test_vid) + RESET)
                #else:
                    #log.info('Test result for ' + RBT + repr(test_vid) + RESET)
            
            #if len(solution_hyp[event][test_vid]) is not 0:
            if len(solution_hyp[event][0]) is not 0:
                sol[event][k_fold_count][test_vid]['test_score'] = [c.score for c in dup_sol_hyp]
            sol[event][k_fold_count][test_vid]['start_end'] = test_prolog_blanket['start_end']
                
        log.info('Total videos test: ' + GBB + repr(total_test_vids) + RESET)
        test_time_end = time.time()
        log.info(GBB + 'TP: ' + RESET + repr(sol[event][k_fold_count]['tp']))
        log.info(RBB + 'FP: ' + RESET + repr(sol[event][k_fold_count]['fp']))        
        log.info(RBB + 'FN: ' + RESET + repr(sol[event][k_fold_count]['fn']))  
        log.info(GBB + 'TN: ' + RESET + repr(sol[event][k_fold_count]['tn']))
        pickle.dump(sol, open(sol_file, 'w'), 1)   
        log.info('Dumped solution file: ' + sol_file)
        log.info(BBB + 'Total Evaluation time for pos ex:' + RESET + repr(pos_eval_time))
        log.info(BBB + 'Total Evaluation time for neg ex:' + RESET + repr(neg_eval_time))
        log.info(BBB + 'Total evaluation time for ' + repr(total_test_vids) + ' vids:' + RESET + repr(test_time_end - test_time_start))
        log.info('')
        log.info('')
        return sol
    except Exception:
        import traceback
        err_msg = traceback.format_exc(sys.exc_info())
        log.error(RBB + 'ERROR' + RESET)
        log.error(RBT + err_msg + RESET)        

if __name__ == "__main__":
    import os, sys
    #learn_event_par_worker(sys.argv)
    input_dir = os.path.join(os.environ['CAD_DATA_DIR'],'prolog_rel_data')
    learn_rel_models(input_dir)
    
